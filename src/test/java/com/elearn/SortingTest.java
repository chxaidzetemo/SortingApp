package com.elearn;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * test sort method
 */
public class SortingTest {
    Sorting sorting = new Sorting();

    @Test(expected = IllegalArgumentException.class)
    public void testNullInput() {
        int[] array = null;
        sorting.sort(array);
    }

    @Test(expected = IllegalArgumentException.class)
    public void arrayLengthOverFlow() {
        int[] arr = {4, 2, 1, 3, 1, 7, 0, 3, 6, 8, 11};
        sorting.sort(arr);
    }

    @Test
    public void testEmptyCase() {
        int[] arr = new int[0];
        sorting.sort(arr);
        assertEquals(0, arr.length);
    }

    @Test
    public void testSingleElementArrayCase() {
        int[] num = new int[]{10};
        sorting.sort(num);
        assertEquals(1, num.length);
        assertEquals(10, num[0]);
    }

    @Test
    public void testSortedArraysCase() {
        int[] sorted = {1, 2, 3};
        sorting.sort(sorted);
        assertArrayEquals(sorted, new int[]{1, 2, 3});
    }

    @Test
    public void testOtherCases() {
        int[] arr = {4, 2, 1, 3, 6, 2, 8, 4, 3, 9};
        sorting.sort(arr);
        assertArrayEquals(arr, new int[]{1, 2, 2, 3, 3, 4, 4, 6, 8, 9});
    }


}