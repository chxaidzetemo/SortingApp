package com.elearn;

import org.junit.Before;
import org.junit.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * test main method
 */
public class AppTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @Test(expected = IllegalArgumentException.class)
    public void arrayLengthOverFlow() {
        App.main(new String[]{"4, 2, 1, 3, 1, 7, 0, 3, 6, 8, 11"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidCharacter() {
        App.main(new String[]{"4, 2, w"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidNumber() {
        App.main(new String[]{"4, 2, 2147483650"});
    }

    @Test
    public void testEmptyInput() {
        App.main(new String[0]);
        assertEquals("", outContent.toString());
    }

    @Test
    public void testSingleInput() {
        App.main(new String[]{"1"});
        assertEquals("1 ", outContent.toString());
    }

    @Test
    public void testSortedArraysCase() {
        App.main(new String[]{"1", "2", "3"});
        assertEquals("1 2 3 ", outContent.toString());
    }

    @Test
    public void testOtherCases() {
        App.main(new String[]{"1", "3", "2", "7", "5", "1", "4", "9", "5", "5"});
        assertEquals("1 1 2 3 4 5 5 5 7 9 ", outContent.toString());
    }
}