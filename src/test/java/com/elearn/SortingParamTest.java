package com.elearn;


import org.junit.Assert;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

import java.util.stream.Stream;

/**
 * test sort method with parameters
 */
@RunWith(Parameterized.class)
public class SortingParamTest {
    Sorting sorting = new Sorting();

    private int[] array;
    private int[] expected;

    /**
     *
     * @param array actual array
     * @param expected expected array
     */
    public SortingParamTest(int[] array, int[] expected) {
        this.array = array;
        this.expected = expected;
    }

    @org.junit.Test
    public void testSortCases() {
        sorting.sort(array);
        Assert.assertThat(array, is(expected));
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() throws IOException {
        int[][] actualNums;
        int[][] expectedNums = {
                {0, 1, 2, 2, 4, 4, 6, 7, 8, 8},
                {1, 2},
                {1, 2, 3},
                {1, 4, 4, 6, 8, 9},
                {1, 2, 3, 4, 5, 6, 7, 8, 9, 9},
                {1, 4, 6, 8, 9}
        };
        try{
            actualNums = readFile();
        }catch (NumberFormatException exception){
            throw new IllegalArgumentException();
        }
        Object[][] objects = new Object[expectedNums.length][2];

        int k = 0, l = 0;
        for (int i = 0; i < objects.length; i++, k++, l++) {
            objects[i][0] = new int[actualNums[k].length];
            objects[i][0] = actualNums[k];

            objects[i][1] = new int[expectedNums[l].length];
            objects[i][1] = expectedNums[l];
        }

        return Arrays.asList(objects);
    }

    public static int[][] readFile() throws IOException {
        List<String> numStrings = Files.readAllLines(Paths.get("src/main/resources/array-sort.txt"));
        int[][] numArrays = new int[10][];
        for (int i = 0; i < numStrings.size(); i++) {
            String[] numCharacters = numStrings.get(i).split(" ");
            int[] nums = Stream.of(numCharacters)
                    .mapToInt(Integer::parseInt).toArray();
            numArrays[i] = nums;
        }
        return numArrays;
    }
}